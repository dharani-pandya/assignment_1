This is "TO DO List Application". It allows to create to do list. It also allows to delete & update your to do list tasks.

Usage:

To run the TO DO Application run index.html file first. It will launch in your local host machine.

I am testing revert functionality of Bitbucket.
License:
This application is maintained under MIT License. This license allows users to copy, reuse, modify, distribute and publish this application.
Since this is a public repository and the application of fairly common knowledge to software community, It is reasonable to keep this under MIT License.

This project is set up for passwordless ssh between VS Code and Bitbucket.

